# SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# An educational city building game for children in primary education.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

{ pkgs, lib, config, inputs, ... }:

{
  # [Android - devenv](https://devenv.sh/integrations/android/)
  android = {
    enable = true;
    platforms.version = [ "33" ];
    systemImageTypes = [ ];
    abis = [ "x86_64" ];
    emulator = {
      enable = false;
    };
    sources.enable = false;
    systemImages.enable = false;
    ndk.enable = true;
    googleAPIs.enable = false;
    googleTVAddOns.enable = false;
    extras = [ ];
    extraLicenses = [
      "android-sdk-preview-license"
      "google-gdk-license"
      "intel-android-extra-license"
      "intel-android-sysimage-license"
    ];
    android-studio.enable = false;
  };
  packages = [ pkgs.ninja
               pkgs.gitleaks 
               pkgs.reuse
               pkgs.curlMinimal
               # Following are dev tools, not dependencies
               pkgs.git
               pkgs.copier
               pkgs.gitlab-ci-local
               ];

  env.BUILDDIR = lib.mkDefault "build";
  enterShell = ''
    # Feed environment to ninja
    echo "builddir="$BUILDDIR"" > .ninja.env
    # Prevent devenv setting ndk.dir, causes errors.
    # https://stackoverflow.com/a/64372652
    sed -i '/ndk.dir/d' local.properties
    # Configure gitleaks
    rm -f .gitleaks.toml
    while IFS= read -r line; do
      line=''${line/\$USER/$USER}
      echo "$line" >> ".gitleaks.toml"
    done <".gitleaks.toml.in"
  '';

  pre-commit.hooks.gitleaks = {
      # https://github.com/gitleaks/gitleaks/blob/master/.pre-commit-hooks.yaml
      enable = true;
      name = "Gitleaks";
      entry = "gitleaks protect --verbose --redact --staged";
      language = "golang";
      pass_filenames = false;
    };
  pre-commit.hooks.reuse = {
      enable = true;
      name = "REUSE";
      entry = "reuse lint";
      pass_filenames = false;
  };
}
