<!--
SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

An educational city building game for children in primary education.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Contributing

<!--toc:start-->

- [Contributing](#contributing)
  - [General Information](#general-information)
  - [Contributions](#contributions)
  - [Vulnerability Reporting or Security Issues](#vulnerability-reporting-or-security-issues)
  - [Attribution](#attribution)

<!--toc:end-->

Note that this repository does not accept pull requests! The code here is provided
in hopes that others may find it useful for their own projects, not to allow
community contribution. Issue reports of all kinds (bug reports, feature requests,
etc.) are welcome.

The rest of this doc details the background and conditions of contributing.
For advice on setting up your machine see [docs/DeveloperSetup.md](docs/DeveloperSetup.md).

## General Information

Please provide ideas via issues to our GitLab repository:
[https://gitlab.com/thegalagic/learners/innercitylearners](https://gitlab.com/thegalagic/learners/innercitylearners).

Our documentation consists of the following files in the repository:

- CHANGELOG.md
- CODE_OF_CONDUCT.md
- CONTRIBUTING.md (this file)
- README.md
- LICENSES directory
- docs directory

## Contributions

Please note that this project is released with a Contributor Code of Conduct. By
participating in this project you agree to abide by its terms. We use the
[Contributor Convenant version
2.0](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html) a
copy of which is available in the repository: CODE_OF_CONDUCT.md. This code is
the same as used by the Linux kernel and many other projects.

## Vulnerability Reporting or Security Issues

If you find a significant vulnerability, or evidence of one, please send an
email to the security contact that you have such information, and we'll tell
you the next steps. For now, the security contact is: infoNOSPAM@galagic.com
(remove the NOSPAM marker).

Please use an email system that supports hop-to-hop encryption using STARTTLS
when reporting vulnerabilities. Examples of such systems include Gmail,
Outlook.com, and runbox.com. See STARTTLS Everywhere if you wish to learn more
about efforts to encourage the use of STARTTLS. Your email client should use
encryption to communicate with your email system (i.e., if you use a web-based
email client then use HTTPS, and if you use email client software then configure
it to use encryption). Hop-to-hop encryption isn't as strong as end-to-end
encryption, but we've decided that it's strong enough for this purpose and it's
much easier to get everyone to use it.

We will gladly give credit to anyone who reports a vulnerability so that we can
fix it. If you want to remain anonymous or pseudonymous instead, please let us
know that; we will gladly respect your wishes.

## Attribution

Parts of this text are based on the contribution guide of the Core
Infrastructure Initiative's
[Best Practices Badge
Project](https://github.com/coreinfrastructure/best-practices-badge/blob/master/CONTRIBUTING.md),
licensed under the [Creative Commons Attribution 3.0 International (CC BY 3.0)
license or later.](https://creativecommons.org/licenses/by/3.0/):

Specifically the following sections were copied and adapted: the introduction,
'General information' and 'Vulnerability Reporting'.
