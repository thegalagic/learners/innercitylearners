<!--
SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

An educational city building game for children in primary education.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Inner City Learners

Inner City Learners is an educational city building game for children in primary
education ([https://innercitylearners.com](https://innercitylearners.com)).
[This](https://gitlab.com/thegalagic/learners/innercitylearners) is the source
code repository for the game.

This software is under active development, expect bugs and breaking changes!

## Screenshots

<img src="docs/screenshots/title-menu.png" width="140" height="297">
<img src="docs/screenshots/game-start.png" width="140" height="297">
<img src="docs/screenshots/memory-bank.png" width="140" height="297">
<img src="docs/screenshots/five-times-table-complete.png" width="140" height="297">
<img src="docs/screenshots/place-house.png" width="140" height="297">
<img src="docs/screenshots/road-extended.png" width="140" height="297">

## Install

We are not yet available in app stores but coming soon! In the meantime you can
try our latest **development** builds:

- For **Linux, Windows and MacOS** download the latest desktop JAR
  [here](https://gitlab.com/thegalagic/learners/innercitylearners/-/jobs/artifacts/main/raw/desktop/build/libs/innercitylearners.jar?job=build).
  Either double-click the download or use the command line:
  `java -jar innercitylearners.jar`. Your must have Java 21+ already installed
  for this to work.
- For **Android** download the latest APK
  [here](https://gitlab.com/thegalagic/learners/innercitylearners/-/jobs/artifacts/main/raw/android/build/outputs/apk/release/innercitylearners.apk?job=build).

## Thanks

Thanks go to the following for their game assets:

- [Kenney](https://kenney.nl/)
- [LYASeeK](https://lyaseek.itch.io/)

## Contributing

Note that this repository does not accept pull requests! The code here is provided
in hopes that others may find it useful for their own projects, not to allow
community contribution. Issue reports of all kinds (bug reports, feature requests,
etc.) are welcome. Please see `CONTRIBUTING.md` for more info.

## Licensing

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

- All source code is licensed under the
  [GNU Affero General Public License v3.0 or later (AGPL v3.0+)](https://www.gnu.org/licenses/agpl.txt).
- All configuration including all text when extracted from code, is licensed
  under
  [Creative Commons Attribution Share Alike 4.0 International (CC-BY-SA-4.0)](https://creativecommons.org/licenses/by-sa/4.0/legalcode).
- All source files that are stating a viewpoint are licensed under
  [Creative Commons Attribution-NoDerivatives 4.0 International](https://creativecommons.org/licenses/by-nd/4.0/).
  All images unless otherwise noted are also under this license.
- Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.
