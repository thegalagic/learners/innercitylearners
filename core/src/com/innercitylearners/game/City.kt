// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class City(viewConfig: ViewConfig,
           private val cityPresenter: CityPresenter,
           private val stars: Stars) {

    private var running: Boolean = true
    private var buildings: List<Building>

    init {
        cityPresenter.cityControls(this::cityRunning)
        buildings = viewConfig.loadBuildings()
        cityPresenter.availableBuildings(buildings)
        cityPresenter.registerBuildingRequested(this::buildSomething)
        cityPresenter.addEligibleSite(110f,166f, 7f, 7f)
    }

    private fun cityRunning(enabled: Boolean) {
        this.running = enabled
    }

    private fun buildSomething(x: Int, y:Int, building: Building) {
        stars.earned(-2)

        // We ought to massage touch x, y here into permissible spot in eligible site
        // Hardcode to centre, bottom of site for now
        val width = building.build(cityPresenter, 113, 166)
        Road(cityPresenter, 113, 166, width)
        cityPresenter.removeEligibleSites()
    }
}
