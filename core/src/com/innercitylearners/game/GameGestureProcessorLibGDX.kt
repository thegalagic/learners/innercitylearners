// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.input.GestureDetector.GestureAdapter

class GameGestureProcessorLibGDX(private val cityView: CityView) : GestureAdapter() {
    override fun pan(x: Float, y: Float, deltaX: Float, deltaY: Float): Boolean {
        cityView.translate(-deltaX, deltaY)
        return true
    }
    override fun touchDown(x: Float, y: Float, pointer: Int, button: Int): Boolean {
        cityView.zoomStart()
        return true
    }
    override fun tap(x: Float, y: Float, count: Int, button: Int): Boolean {
        cityView.touch(x.toInt(), y.toInt())
        return true
    }

    override fun zoom(initialDistance: Float, distance: Float): Boolean {
        cityView.zoomScale((1 / (distance/initialDistance)))
        return true
    }

}
