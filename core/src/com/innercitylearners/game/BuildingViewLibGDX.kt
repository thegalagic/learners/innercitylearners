// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align

class BuildingViewLibGDX(private val viewConfigLibGDX: ViewConfigLibGDX,
                         private val skin: Skin,
                         private val chosen: (Building) -> Unit) : BuildingView {
    private var buildingsHorizontalGroup: HorizontalGroup
    private lateinit var stageProvider: StageProvider
    private val borderPadding = viewConfigLibGDX.styleRem(0.7f)
    private var table = Table()
    private val exitButton = TextButton(
        "X",
        skin,
        viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonLearningGroup)
    )

    init {
        table.setFillParent(true)
        table.pad(borderPadding)
        table.background = skin.getDrawable("color-black-80")

        exitButton.pad(
            viewConfigLibGDX.styleRem(0.2f),
            viewConfigLibGDX.styleRem(1.25f),
            viewConfigLibGDX.styleRem(0.5f),
            viewConfigLibGDX.styleRem(1.25f)
        )

        table.add(exitButton).right().padBottom(viewConfigLibGDX.styleRem(0.7f))
        table.row()

        buildingsHorizontalGroup = HorizontalGroup()
        buildingsHorizontalGroup.align(Align.bottomLeft)
        buildingsHorizontalGroup.expand()
        buildingsHorizontalGroup.fill()
        buildingsHorizontalGroup.pad(viewConfigLibGDX.styleRem(0.7f), 0f, 0f, 0f)
        buildingsHorizontalGroup.rowAlign(Align.left)
        buildingsHorizontalGroup.wrap()

        // ScrollPanes don't do alignment, so we have to wrap their content in another table
        // and specify growing cells in that to put our real content where we want it.
        val wrapBuildingsGroup = Table()
        wrapBuildingsGroup.add(buildingsHorizontalGroup).growX()
        wrapBuildingsGroup.row()
        wrapBuildingsGroup.add().growY()

        val taskScrollPane = ScrollPane(wrapBuildingsGroup, skin)
        taskScrollPane.setClamp(true)
        taskScrollPane.setOverscroll(true, false)
        taskScrollPane.setupOverscroll(
            viewConfigLibGDX.styleRem(1f),
            viewConfigLibGDX.styleRem(1f),
            viewConfigLibGDX.styleRem(7f))

        table.add(taskScrollPane).grow()
    }

    private fun addBuildingToView(building: Building, name: String) {
        val texture = viewConfigLibGDX.getTexture(building)
        val houseButton = Image(texture)

        val placeButton = TextButton(
            "PLACE",
            skin,
            viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonDefault)
        )
        placeButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                chosen(building)
            }
        })
        placeButton.pad(viewConfigLibGDX.styleRem(1.3f), viewConfigLibGDX.styleRem(1.3f), viewConfigLibGDX.styleRem(1f), viewConfigLibGDX.styleRem(1.3f))

        val infoTable = Table()
        infoTable.add(Label(name, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelGameBody )))
            .padBottom(borderPadding)
            .align(Align.left)
        infoTable.row()
        infoTable.add(placeButton).align(Align.bottomLeft)

        val buildingTable = Table()
        buildingTable.align(Align.bottom)
        buildingTable.pad(borderPadding)
        buildingTable.add(houseButton).padRight(borderPadding)
        buildingTable.add(infoTable).align(Align.bottomLeft)
        buildingsHorizontalGroup.addActor(buildingTable)
    }

    override fun addMyViewParent(stageProvider: StageProvider) {
        this.stageProvider = stageProvider
    }

    override fun hide() {
        buildingsHorizontalGroup.clear()
        table.remove()
    }

    override fun show() {
        stageProvider.addTable(table)
    }

    override fun addBuilding(building: Building, name: String) {
        addBuildingToView(building, name)
    }

    override fun addExit(finished: () -> Unit) {
        exitButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                finished()
            }
        })
    }
}