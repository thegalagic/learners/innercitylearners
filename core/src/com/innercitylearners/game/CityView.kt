// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

interface CityView {
    fun zoom(delta: Float)
    fun zoomStart()
    fun zoomScale(scale: Float)
    fun translate(deltaX: Float, deltaY: Float)
    fun touch(screenX: Int, screenY: Int)
    fun build(building: Building, x: Int, y: Int)
    fun showBuildSites()
    fun hideBuildSites()
    fun acceptPlayerInput(enabled: Boolean)
    fun addEligibleSite(x: Float, y: Float, width: Float, height: Float)
    fun removeEligibleSites()
    fun buildRoad(x: Int, y: Int, width: Int)
}
