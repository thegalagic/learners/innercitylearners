// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.input.GestureDetector
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

class CityViewLibGDX(private val viewConfigLibGDX: ViewConfigLibGDX,
                     private val buildAction: (Int, Int) -> Unit) : CityView {
    private val inputAdapter = when(Gdx.app.type) {
        Application.ApplicationType.Android ->
            GestureDetector(GameGestureProcessorLibGDX(this))
        else -> GameInputProcessorLibGDX(this)
    }
    private lateinit var gameInputMultiplexer: GameInputMultiplexerLibGDX
    private var camera: OrthographicCamera = OrthographicCamera()
    private var batch: SpriteBatch = SpriteBatch()
    private var map: TiledMap = TmxMapLoader().load("Green.tmx")
    private var renderer: OrthogonalTiledMapRenderer
    private var zoomStart : Float = 0f
    private val zoomRange = Pair(0.005f, .1f)
    private val layerBuilding0 = TiledMapTileLayer(225, 338, 16, 16)
    private val layerBuilding1 = TiledMapTileLayer(225, 338, 16, 16)
    private val layerOverlays0 = TiledMapTileLayer(225, 338, 16, 16)
    private val eligibleSites = mutableListOf<Rectangle>()
    private val roadSites = mutableListOf<Rectangle>()
    private val roadControls = mutableMapOf<Cell,()->Unit>()

    private var showingEligibleBuildSites = false
    private lateinit var crossHair: Sprite

    init {
        map.layers.add(layerBuilding0)
        map.layers.add(layerBuilding1)
        map.layers.add(layerOverlays0)

        renderer = OrthogonalTiledMapRenderer(map, 1/16f)
        camera.setToOrtho(false)

        // Put camera in the middle
        val mapWidth = (map.properties.get("width") as Int).toFloat()
        val mapHeight = (map.properties.get("height") as Int).toFloat()
        camera.position.add(mapWidth/2f +.5f, mapHeight/2f + .5f, 0f)
        camera.position.sub(camera.viewportWidth/2, camera.viewportHeight/2, 0f)
        // This starting zoom might need to scale with device dpi or something
        // Makes sense on Android but a bit big on desktop
        camera.zoom = .015f
        renderer.setView(camera)
    }

    fun render() {
        camera.update()
        renderer.setView(camera)
        renderer.render()

        batch.projectionMatrix = camera.combined
        batch.begin()
        if(showingEligibleBuildSites and eligibleSites.any()) {
            crossHair.draw(batch)
        }
        batch.end()
    }

    override fun zoom(delta: Float) {
        camera.zoom += delta
        camera.zoom = MathUtils.clamp(camera.zoom, zoomRange.first, zoomRange.second)
    }

    override fun zoomStart() {
        this.zoomStart = camera.zoom
    }

    override fun zoomScale(scale: Float) {
        camera.zoom = this.zoomStart * scale
        camera.zoom = MathUtils.clamp(camera.zoom, zoomRange.first, zoomRange.second)
    }

    override fun translate(deltaX: Float, deltaY: Float) {
        camera.translate(Vector3(deltaX, deltaY, 0f).scl(camera.zoom))
    }

    override fun touch(screenX: Int, screenY: Int) {
        val worldTouch = camera.unproject(
            Vector3(
                screenX.toFloat(),
                screenY.toFloat(),
                0f
            )
        )

        if (showingEligibleBuildSites) {
            if (eligibleSites.any { it.contains(Vector2(worldTouch.x, worldTouch.y)) }) {
                buildAction(worldTouch.x.toInt(), worldTouch.y.toInt())
            } else {
                roadControls[layerOverlays0.getCell(
                    worldTouch.x.toInt(),
                    worldTouch.y.toInt()
                )]?.invoke()
            }
        }
    }

    override fun build(building: Building, x: Int, y: Int) {
        val tiles = viewConfigLibGDX.getTiles(building)
        addBuilding(tiles.first, tiles.second, x, y)
    }

    override fun addEligibleSite(x: Float, y: Float, width: Float, height: Float) {
        eligibleSites.add(Rectangle(x,y,width,height))
    }

    override fun removeEligibleSites() {
        val allSites = eligibleSites
        for(eligibleSite in allSites) {
            for (x in eligibleSite.x.toInt()..(eligibleSite.x + eligibleSite.width).toInt()) {
                for (y in eligibleSite.y.toInt()..(eligibleSite.y + eligibleSite.height).toInt()) {
                    layerOverlays0.setCell(x, y, null)
                }
            }
        }
        eligibleSites.clear()
    }

    override fun buildRoad(x: Int, y: Int, width: Int) {
        var x = x
        if (width >= 3) {
            x -= (width - 1)/2
        }
        val newRoadSite = Rectangle(x.toFloat(), (y-7).toFloat(), width.toFloat(), 6f)
        roadSites.add(newRoadSite)
        addRoadSiteControls(newRoadSite)

        for (i in 0..<width) {
            drawRoadStrip(x + i, y)
        }
    }

    private fun drawRoadStrip(x: Int, y: Int, clear: Boolean = false) {
        val pavement = if (clear) null else Cell().setTile(map.tileSets.getTile(748))
        val topCurb = if (clear) null else Cell().setTile(map.tileSets.getTile(822))
        val bottomCurb = if (clear) null else Cell().setTile(map.tileSets.getTile(711))
        val road = if (clear) null else Cell().setTile(map.tileSets.getTile(715))
        layerBuilding0.setCell(x, y - 1, pavement)
        layerBuilding0.setCell(x, y - 2, topCurb)
        layerBuilding0.setCell(x, y - 3, road)
        layerBuilding0.setCell(x, y - 4, road)
        layerBuilding0.setCell(x , y - 5, road)
        layerBuilding0.setCell(x , y - 6, bottomCurb)
        layerBuilding0.setCell(x , y - 7, pavement)
    }

    override fun showBuildSites() {
        showingEligibleBuildSites = true
        val tileCoOrds : MutableList<Pair<Vector2, Int>> = mutableListOf()

        for(eligibleSite in eligibleSites) {
            // Bottom left
            tileCoOrds.add(Pair(Vector2(eligibleSite.x, eligibleSite.y), 1044))
            // Top left
            tileCoOrds.add(
                Pair(
                    Vector2(eligibleSite.x, eligibleSite.y + eligibleSite.height - 1),
                    1038
                )
            )
            // Bottom right
            tileCoOrds.add(
                Pair(
                    Vector2(eligibleSite.x + eligibleSite.width - 1, eligibleSite.y),
                    1042
                )
            )
            // Top right
            tileCoOrds.add(
                Pair(
                    Vector2(
                        eligibleSite.x + eligibleSite.width - 1,
                        eligibleSite.y + eligibleSite.height - 1
                    ), 1040
                )
            )

            // Bottom and top
            for (x in eligibleSite.x.toInt() + 1..(eligibleSite.x + eligibleSite.width - 2).toInt()) {
                // Bottom
                tileCoOrds.add(Pair(Vector2(x.toFloat(), eligibleSite.y), 1043))
                // Top
                tileCoOrds.add(
                    Pair(
                        Vector2(x.toFloat(), eligibleSite.y + eligibleSite.height - 1),
                        1039
                    )
                )
            }
            // Sides
            for (y in eligibleSite.y.toInt() + 1..(eligibleSite.y + eligibleSite.height - 2).toInt()) {
                // Left
                tileCoOrds.add(Pair(Vector2(eligibleSite.x, y.toFloat()), 1037))
                // Right
                tileCoOrds.add(
                    Pair(
                        Vector2(eligibleSite.x + eligibleSite.width - 1, y.toFloat()),
                        1041
                    )
                )
            }

            // TODO Obviously this won't work for multiple sites
            crossHair = Sprite(map.tileSets.getTile(1045).textureRegion)
            crossHair.setPosition(eligibleSite.x + (eligibleSite.width - 1)/2, eligibleSite.y + (eligibleSite.height - 1)/2)
            crossHair.setSize(1f, 1f)
        }

        for (tileCoOrd in tileCoOrds) {
            val cell = Cell()
            cell.setTile(map.tileSets.getTile(tileCoOrd.second))
            layerOverlays0.setCell(tileCoOrd.first.x.toInt(), tileCoOrd.first.y.toInt(), cell)
        }

        // Roads
        for(roadSite in roadSites) {
            addRoadSiteControls(roadSite)
        }
    }

    private fun addRoadSiteControls(roadSite: Rectangle, clear: Boolean = false) {
        val arrowExpandWest = Cell().setTile(map.tileSets.getTile(1046))
        val arrowShrinkWest = Cell().setTile(map.tileSets.getTile(1047))
        val arrowShrinkEast = Cell().setTile(map.tileSets.getTile(1046))
        val arrowExpandEast = Cell().setTile(map.tileSets.getTile(1047))

        if (clear) {
            roadControls.clear()
        } else {
            roadControls[arrowExpandWest] = { alterRoad(roadSite, west = true, shrink = false) }
            roadControls[arrowShrinkWest] = { alterRoad(roadSite, west = true, shrink = true) }
            roadControls[arrowShrinkEast] = { alterRoad(roadSite, west = false, shrink = true) }
            roadControls[arrowExpandEast] = { alterRoad(roadSite, west = false, shrink = false) }
        }

        if(roadSite.width > 3) {
            layerOverlays0.setCell(
                roadSite.x.toInt(),
                roadSite.y.toInt() + 2,
                if (clear) null else arrowShrinkWest
            )
            layerOverlays0.setCell(
                roadSite.x.toInt() + roadSite.width.toInt() - 1,
                roadSite.y.toInt() + 2,
                if (clear) null else arrowShrinkEast
            )
        }
        layerOverlays0.setCell(roadSite.x.toInt(), roadSite.y.toInt() + 4, if (clear) null else arrowExpandWest)
        layerOverlays0.setCell(
            roadSite.x.toInt() + roadSite.width.toInt() - 1,
            roadSite.y.toInt() + 4,
            if (clear) null else arrowExpandEast
        )
    }

    private fun alterRoad(roadSite: Rectangle, west: Boolean, shrink: Boolean) {
        addRoadSiteControls(roadSite, clear = true)
        val x = if (west) roadSite.x.toInt() else roadSite.x.toInt() + roadSite.width.toInt() - 1

        if (shrink) {
            if (west) {
                drawRoadStrip(x, roadSite.y.toInt() + 7, clear = true)
                roadSite.x += 1
                roadSite.width -= 1
            } else {
                drawRoadStrip(x, roadSite.y.toInt() + 7, clear = true)
                roadSite.width -= 1
            }
        } else {
            if (west) {
                drawRoadStrip(x - 1, roadSite.y.toInt() + 7)
                roadSite.x -= 1
                roadSite.width += 1
            } else {
                drawRoadStrip(x + 1, roadSite.y.toInt() + 7)
                roadSite.width += 1
            }
        }

        addRoadSiteControls(roadSite)
    }

    override fun hideBuildSites() {
        showingEligibleBuildSites = false
        val allSites = eligibleSites + roadSites
        for(eligibleSite in allSites) {
            for (x in eligibleSite.x.toInt()..(eligibleSite.x + eligibleSite.width).toInt()) {
                for (y in eligibleSite.y.toInt()..(eligibleSite.y + eligibleSite.height).toInt()) {
                    layerOverlays0.setCell(x, y, null)
                }
            }
        }
    }

    private fun addBuilding(bottomTiles: Array<Array<out Int?>>,
                            topTiles: Array<Array<out Int?>>,
                            x: Int,
                            y: Int) {
        // x and y are the bottom centre of where to build
        var startx = x
        if (bottomTiles.size >= 3) {
            startx -= (bottomTiles.size - 1)/2
        }
        val starty = y

        for (i in bottomTiles.indices) {
            for (y in bottomTiles[i].indices) {
                val tile = bottomTiles[i][y]
                if (tile != null) {
                    val cell = Cell()
                    cell.setTile(map.tileSets.getTile(tile))
                    layerBuilding0.setCell(startx + i, starty + y, cell)
                }
            }
        }

        for (i in topTiles.indices) {
            for (y in topTiles[i].indices) {
                val tile = topTiles[i][y]
                val cell = Cell()
                if (tile != null) {
                    cell.setTile(map.tileSets.getTile(tile))
                }
                layerBuilding1.setCell(startx + i, starty + y, cell)
            }
        }
    }

    override fun acceptPlayerInput(enabled: Boolean) {
        if (enabled) {
            gameInputMultiplexer.addProcessor(inputAdapter)
        } else {
            gameInputMultiplexer.removeProcessor(inputAdapter)
        }

    }

    fun addGameInputMultiplexer(gameInputMultiplexerLibGDX: GameInputMultiplexerLibGDX) {
        gameInputMultiplexer = gameInputMultiplexerLibGDX
    }
}