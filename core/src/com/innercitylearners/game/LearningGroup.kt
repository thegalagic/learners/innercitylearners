// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class LearningGroup(private val title : String) {
    private var category: String = String()
    private var description: String = String()
    private val groups: MutableList<LearningGroup> = mutableListOf()
    private val tasks: MutableList<Task> = mutableListOf()

    constructor(title : String,
                category : String = String(),
                description : String) : this(title) {
        this.category = category
        this.description = description
    }

    fun addGroup(learningGroup : LearningGroup) {
        groups.add(learningGroup)
    }

    fun addTask(task : Task) {
        tasks.add(task)
    }

    fun setUp(learningGroupPresenter: LearningGroupPresenter) {
        learningGroupPresenter.reset()
        learningGroupPresenter.backAction( this::setUp )
        learningGroupPresenter.addTitle(title)
        learningGroupPresenter.addDescription(description)

        for(group in groups) {
            group.present(learningGroupPresenter)
        }
        for(task in tasks) {
            task.present(learningGroupPresenter)
        }
    }

    private fun present(learningGroupPresenter: LearningGroupPresenter) {
        if(category.isBlank()) {
            learningGroupPresenter.addGroup(title, description, this::setUp)
        } else {
            learningGroupPresenter.addGroup(title, description, category, this::setUp)
        }
    }

}