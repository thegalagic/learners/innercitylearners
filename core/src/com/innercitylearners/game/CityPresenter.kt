// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class CityPresenter(viewConfig: ViewConfig,
                    private val buildingPresenter: BuildingPresenter) {
    private var x: Int = 0
    private var y: Int = 0
    private var buildings: List<Building> = listOf()
    private val cityView = viewConfig.createCityView(this::build)
    private lateinit var actualCityRunning: (enabled: Boolean) -> Unit
    private lateinit var showGameActions: () -> Unit
    private lateinit var hideGameActions: () -> Unit
    private lateinit var buildingRequested: (Int, Int, Building) -> Unit
    private var hideBuildSites = false

    fun hookup(showGameActions: () -> Unit, hideGameActions: () -> Unit) {
        this.showGameActions = showGameActions
        this.hideGameActions = hideGameActions
    }

    fun cityControls(cityRunning: (enabled: Boolean) -> Unit) {
        this.actualCityRunning = cityRunning
    }

    fun toggleBuild() {
        if(hideBuildSites) {
            cityView.hideBuildSites()
        } else {
            cityView.showBuildSites()
        }
        hideBuildSites = !hideBuildSites
    }

    fun cityRunning(enabled: Boolean) {
        cityView.acceptPlayerInput(enabled)
        actualCityRunning(enabled)
    }

    fun registerBuildingRequested(buildingRequested: (Int, Int, Building) -> Unit) {
        this.buildingRequested = buildingRequested
    }

    fun build(building: Building, x: Int, y: Int) {
        cityView.build(building, x, y)
    }

    private fun build(x: Int, y: Int) {
        this.x = x
        this.y = y
        cityRunning(false)
        for(building in buildings) {
            building.show(buildingPresenter)
        }
        buildingPresenter.show(this::buildingPicked)
    }

    private fun buildingPicked(building: Building) {
        buildingRequested(x, y, building)
    }

    fun availableBuildings(buildings: List<Building>) {
        this.buildings = buildings
    }

    fun addEligibleSite(x: Float, y: Float, width: Float, height: Float) {
        cityView.addEligibleSite(x, y, width, height)
    }

    fun removeEligibleSites() {
        cityView.removeEligibleSites()
    }

    fun buildRoad(x: Int, y: Int, width: Int) {
        cityView.buildRoad(x, y, width)
    }
}
