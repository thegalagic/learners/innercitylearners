// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align


class TitleViewLibGDX(private val viewConfigLibGDX: ViewConfigLibGDX,
                      private val skin: Skin) : Screen, TitleView {

    private val stage: Stage = Stage()
    private val table = Table()
    private val buttonTable = Table()

    init {
        table.setFillParent(true)
        stage.addActor(table)

        val titleTable = Table()
        titleTable.background = skin.getDrawable(viewConfigLibGDX.drawableName(ViewConfigLibGDX.DrawableName.ColorCharcoal))
        titleTable.padBottom(viewConfigLibGDX.styleRem( 2f ))
        titleTable.align(Align.bottom)

        val titleActor = makeTitle()
        titleTable.add(titleActor).align(Align.bottom);

        titleTable.row()
        val label = Label("LEARNERS", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelTitleH2 ))
        titleTable.add(label)
        table.add(titleTable).growX().minHeight(viewConfigLibGDX.styleRem( 12f ))
        table.row()
        val container = Container<Table>()
        container.background = skin.getDrawable(viewConfigLibGDX.drawableName(ViewConfigLibGDX.DrawableName.ColorChateauGreen))
        container.align(Align.top)
        container.padTop(viewConfigLibGDX.styleRem( 2.5f ))
        container.setActor(buttonTable);
        table.add(container).grow();
    }

    override fun addButton(text: String, action: () -> Unit) {
        val button = TextButton(text, skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonDefault))
        button.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                action.invoke()
            }
        })
        button.pad(viewConfigLibGDX.styleRem(1.3f), viewConfigLibGDX.styleRem(2.5f), viewConfigLibGDX.styleRem(1f), viewConfigLibGDX.styleRem(2.5f))
        buttonTable.add(button).padBottom(viewConfigLibGDX.styleRem(1f)).growX()
        buttonTable.row()
    }

    override fun setScreen() {
        Gdx.input.inputProcessor = stage
        viewConfigLibGDX.screen = this
    }

    override fun show() {
    }

    override fun render(delta: Float) {
        stage.act()
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun hide() {
    }

    override fun dispose() {
    }

    private fun makeTitle() : Actor {
        val title = "INNERCITY"
        val stack = Stack()
        var container = Container<Label>()
        container.padBottom(viewConfigLibGDX.styleRem( 0.7f ))
        var label = Label(title, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelTitleH1Shadow ))
        container.setActor(label)
        stack.addActor(container)

        container = Container()
        container.padBottom(viewConfigLibGDX.styleRem( 0.25f ))
        label = Label(title, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelTitleH1Highlight ))
        container.setActor(label)
        stack.addActor(container)

        label = Label(title, skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.LabelTitleH1))
        stack.addActor(label)
        return stack
    }
}
