// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class Task(viewConfig: ViewConfig,
           private val text: String,
           private val stars: Stars,
           backCallback: () -> Unit) {

    private var description: String = String()
    private var questions: List<Triple<Int, Int, Int>> = listOf()
    private val taskPresenter = TaskPresenter(viewConfig, backCallback)
    private var earn: Int = 0
    private var setup: Boolean = false

    constructor(viewConfig: ViewConfig,
                text: String,
                stars: Stars,
                backCallback: () -> Unit,
                description: String) : this(viewConfig, text, stars, backCallback) {
        this.description = description
    }

    fun addQuestion(first: Int, second: Int, answer: Int) : Task {
        questions = questions + Triple(first, second, answer)
        return this
    }
    fun earn(stars: Int) : Task {
        earn = stars
        return this
    }

    private fun setUp() {
        if (!setup) {
            for (question in questions) {
                taskPresenter.addQuestion(question.first, question.second, question.third)
            }
            taskPresenter.addSubmit { answers: List<Int> -> checkAnswers(answers) }
            setup = true
        }
        taskPresenter.show()
    }

    fun present(learningGroupPresenter: LearningGroupPresenter) {
        learningGroupPresenter.addTask(text, description, this::setUp)
    }

    private fun checkAnswers(answers: List<Int>) {
        var correct = true

        for((index, answer) in answers.withIndex()) {
            val answerCorrect = questions[index].third == answer
            taskPresenter.markCorrect(index, answerCorrect)
            correct = correct && answerCorrect
        }

        if(correct) {
            taskPresenter.success()
            stars.earned(earn)
        } else {
            taskPresenter.fail()
        }
    }

}