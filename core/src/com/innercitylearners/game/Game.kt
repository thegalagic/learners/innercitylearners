// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class Game(viewConfig: ViewConfig, exit: () -> Unit) {
    private val stars = Stars(this)
    private val buildingPresenter = BuildingPresenter(viewConfig)
    private val cityPresenter = CityPresenter(viewConfig, buildingPresenter)
    private val city = City(viewConfig, cityPresenter, stars)
    private val learningGroupPresenter = LearningGroupPresenter(viewConfig)
    private val gamePresenter = GamePresenter(viewConfig, exit, cityPresenter::cityRunning,
        cityPresenter::toggleBuild, learningGroupPresenter::show)

    init {
        cityPresenter.hookup({ gamePresenter.showGameActions() }, { gamePresenter.hideGameActions() })
        learningGroupPresenter.exitAction(gamePresenter::resumeGame)
        buildingPresenter.exitAction { cityPresenter.cityRunning(true) }
        setUpLearningGroup(viewConfig)
    }

    fun starsEarned(total: Int, amount: Int) {
        gamePresenter.starsEarned(total, amount)
    }

    private fun setUpLearningGroup(viewConfig: ViewConfig) {
        val groupNatCurEng = LearningGroup("National Curriculum in England")
        val groupMathsYr2 = LearningGroup("Maths", "Year 2", "The beauty and power of mathematics")
        val groupMathsYr3 = LearningGroup("Maths", "Year 3", "The beauty and power of mathematics")
        val groupMathsNumberYr2 = LearningGroup("Number", description = "Working with numbers")
        val groupMathsNumberYr3 = LearningGroup("Number", description = "Working with numbers")
        val groupMultAndDivYr2 = LearningGroup("Multiplication and Division", description = "Facts, statements and tables")
        val groupMultAndDivYr3 = LearningGroup("Multiplication and Division", description = "Facts, statements and tables")

        // Year 2
        groupNatCurEng.addGroup(groupMathsYr2)
        groupMathsYr2.addGroup(groupMathsNumberYr2)
        groupMathsNumberYr2.addGroup(groupMultAndDivYr2)

        // Year 3
        groupNatCurEng.addGroup(groupMathsYr3)
        groupMathsYr3.addGroup(groupMathsNumberYr3)
        groupMathsNumberYr3.addGroup(groupMultAndDivYr3)

        val twoTimesTable = Task(viewConfig, "Two multiplication table", stars, learningGroupPresenter::show)
        twoTimesTable.addQuestion(2, 2, 4)
        twoTimesTable.addQuestion(2, 3, 6)
        twoTimesTable.addQuestion(2, 4, 8)
        twoTimesTable.earn(2)
        val threeTimesTable = Task(viewConfig, "Three multiplication table", stars, learningGroupPresenter::show)
        threeTimesTable.addQuestion(3, 2, 6)
        threeTimesTable.addQuestion(3, 3, 9)
        threeTimesTable.addQuestion(3, 4, 12)
        threeTimesTable.earn(2)
        val fourTimesTable = Task(viewConfig, "Four multiplication table", stars, learningGroupPresenter::show)
        fourTimesTable.addQuestion(4, 2, 8)
        fourTimesTable.addQuestion(4, 3, 12)
        fourTimesTable.addQuestion(4, 4, 16)
        fourTimesTable.earn(3)
        val fiveTimesTable = Task(viewConfig, "Five multiplication table", stars, learningGroupPresenter::show)
        fiveTimesTable.addQuestion(5, 2, 10)
        fiveTimesTable.addQuestion(5, 3, 15)
        fiveTimesTable.addQuestion(5, 4, 20)
        fiveTimesTable.earn(3)
        val eightTimesTable = Task(viewConfig, "Eight multiplication table", stars, learningGroupPresenter::show)
        eightTimesTable.addQuestion(8, 2, 16)
        eightTimesTable.addQuestion(8, 3, 24)
        eightTimesTable.addQuestion(8, 4, 32)
        eightTimesTable.earn(5)
        val tenTimesTable = Task(viewConfig, "Ten multiplication table", stars, learningGroupPresenter::show)
        tenTimesTable.addQuestion(10, 2, 20)
        tenTimesTable.addQuestion(10, 3, 30)
        tenTimesTable.addQuestion(10, 4, 40)
        tenTimesTable.earn(5)

        groupMultAndDivYr2.addTask(twoTimesTable)
        groupMultAndDivYr2.addTask(fiveTimesTable)
        groupMultAndDivYr2.addTask(tenTimesTable)

        groupMultAndDivYr3.addTask(threeTimesTable)
        groupMultAndDivYr3.addTask(fourTimesTable)
        groupMultAndDivYr3.addTask(eightTimesTable)

        groupNatCurEng.setUp(learningGroupPresenter)
    }
}