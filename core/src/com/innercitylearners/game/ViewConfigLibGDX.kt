// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.Hinting
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.maps.objects.RectangleMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.utils.Drawable
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.Json.ReadOnlySerializer
import com.badlogic.gdx.utils.JsonValue
import kotlin.reflect.KClass


class ViewConfigLibGDX : Game(), ViewConfig {

    enum class DrawableName(private val drawableName: String) {
        ColorCharcoal("color-charcoal"),
        ColorChateauGreen("color-chateaugreen"),
        ;

        override fun toString(): String {
            return drawableName
        }
    }

    enum class StyleName(private val styleName: String, val clazz: KClass<out Any>) {
        ImageButtonIconBuild("icon_build", ImageButtonStyle::class),
        ImageButtonIconSettings("icon_settings", ImageButtonStyle::class),
        ImageButtonIconMemoryBank("icon_memorybank", ImageButtonStyle::class),
        LabelGameBody("game_body", LabelStyle::class),
        LabelGameH1("game_h1", LabelStyle::class),
        LabelLearningGroupBody("learninggroup_body", LabelStyle::class),
        LabelLearningGroupH1("learninggroup_h1", LabelStyle::class),
        LabelLearningGroupH2("learninggroup_h2", LabelStyle::class),
        LabelTitleH2("title_h2", LabelStyle::class),
        LabelTitleH1("title_h1", LabelStyle::class),
        LabelTitleH1Highlight("title_h1_highlight", LabelStyle::class),
        LabelTitleH1Shadow("title_h1_shadow", LabelStyle::class),
        TextButtonDefault("default", TextButton.TextButtonStyle::class),
        TextButtonLearningGroup("learninggroup", TextButton.TextButtonStyle::class),
        TextFieldTaskBody("task_body", TextField.TextFieldStyle::class),
        TextureIconHeart("icon_heart", TextureRegion::class),
        TextureIconStar("icon_star", TextureRegion::class),
        ;

        override fun toString(): String {
            return styleName
        }
    }

    private val buildings: MutableList<Building> = mutableListOf()
    private val buildingTextures: MutableMap<Building, TextureRegion> = mutableMapOf()
    private val buildingTiles: MutableMap<Building, Pair<Array<Array<out Int?>>, Array<Array<out Int?>>>> = mutableMapOf()
    private lateinit var screenGrab: TextureRegion
    private lateinit var displayDesignation: DisplayDesignation
    private lateinit var learningPlanViewLibGDX: LearningGroupViewLibGDX
    private lateinit var buildingViewLibGDX: BuildingViewLibGDX
    private lateinit var cityViewLibGDX: CityViewLibGDX
    private lateinit var gameViewLibGDX: GameViewLibGDX
    private lateinit var skin: Skin

    private enum class DisplayDesignation {
        ZERO_23M9 {
            override fun codename() = "moon"
            override fun factor() = 1f
        },
        FHD {
            override fun codename() = "io"
            override fun factor() = 1.6875f
        },
        TWO_59M09 {
            override fun codename() = "phobos"
            override fun factor() = 3f
        },

        ;

        abstract fun codename(): String
        abstract fun factor(): Float

        fun fits(width: Int, height: Int): Boolean {
            return width >= 360 * factor() && height >= 640 * factor()
        }

        fun styleName(style: String): String {
            return "${codename()}-${style}"
        }

        fun rem(scale: Float): Float {
            return scale * 17f * factor()
        }

        fun zoom(pixelZoom: Float): Float {
            // This is based on a camera zoom of 1/16th == pixel perfect
            // And on io desktop our factor = 27/16
            // And we want to supersize our houses 4x to match our fonts 4x appearance
            // so magic number is 4 / (27/16) = 64/27
            return pixelZoom / ((64/27) * factor())
        }
    }

    override fun createTitleView(): TitleView {
        return TitleViewLibGDX(this, skin)
    }

    override fun createGameView(exit: () -> Unit): GameView {
        gameViewLibGDX = GameViewLibGDX(
            this,
            skin,
            { mapRender() },
            exit
        )
        cityViewLibGDX.addGameInputMultiplexer(gameViewLibGDX)
        learningPlanViewLibGDX.addMyViewParent(gameViewLibGDX)
        buildingViewLibGDX.addMyViewParent(gameViewLibGDX)

        return gameViewLibGDX
    }

    override fun createCityView(buildAction: (Int, Int) -> Unit): CityView {
        cityViewLibGDX = CityViewLibGDX(this, buildAction)
        return cityViewLibGDX
    }

    override fun createLearningPlanView(): LearningGroupView {
        learningPlanViewLibGDX = LearningGroupViewLibGDX(this, skin)
        return learningPlanViewLibGDX
    }

    override fun createBuildingView(chosen: (Building) -> Unit): BuildingView {
        buildingViewLibGDX = BuildingViewLibGDX(this, skin, chosen)
        return buildingViewLibGDX
    }

    override fun createTaskView(): TaskView {
        val taskViewLibGDX = TaskViewLibGDX(this, skin, gameViewLibGDX)
        return taskViewLibGDX
    }

    override fun create() {
        skin = loadSkin()
        checkSkin()
        displayDesignation = chooseDisplayDesignation()
        createBuildings()
        Launcher(this)
    }

    override fun quit() {
        Gdx.app.exit()
    }

    override fun loadBuildings(): List<Building> {
        return buildings
    }

    fun styleRem(scale: Float): Float {
        return displayDesignation.rem(scale)
    }

    fun styleName(style: StyleName): String {
        return displayDesignation.styleName(style.toString())
    }

    fun drawableName(drawableName: DrawableName): String {
        return drawableName.toString()
    }

    // Wiring methods

    private fun mapRender() {
        cityViewLibGDX.render()
    }

    private fun loadSkin(): Skin {
        // https://github.com/raeleus/skin-composer/wiki/Creating-FreeType-Fonts#using-a-custom-serializer
        return object : Skin(Gdx.files.internal("skin/skin.json")) {
            //Override json loader to process FreeType fonts from skin JSON
            override fun getJsonLoader(skinFile: FileHandle): Json {
                val json = super.getJsonLoader(skinFile)
                val skin: Skin = this

                json.setSerializer(
                    FreeTypeFontGenerator::class.java,
                    object : ReadOnlySerializer<FreeTypeFontGenerator?>() {
                        override fun read(
                            json: Json,
                            jsonData: JsonValue, type: Class<*>?
                        ): FreeTypeFontGenerator? {
                            val path = json.readValue(
                                "font",
                                String::class.java, jsonData
                            )
                            jsonData.remove("font")

                            val hinting = Hinting.valueOf(
                                json.readValue(
                                    "hinting",
                                    String::class.java, "AutoMedium", jsonData
                                )
                            )
                            jsonData.remove("hinting")

                            val minFilter = TextureFilter.valueOf(
                                json.readValue(
                                    "minFilter",
                                    String::class.java, "Nearest", jsonData
                                )
                            )
                            jsonData.remove("minFilter")

                            val magFilter = TextureFilter.valueOf(
                                json.readValue(
                                    "magFilter",
                                    String::class.java, "Nearest", jsonData
                                )
                            )
                            jsonData.remove("magFilter")

                            val parameter = json.readValue(
                                FreeTypeFontParameter::class.java, jsonData
                            )
                            parameter.hinting = hinting
                            parameter.minFilter = minFilter
                            parameter.magFilter = magFilter
                            val generator = FreeTypeFontGenerator(skinFile.parent().child(path))
                            val font = generator.generateFont(parameter)
                            skin.add(jsonData.name, font)
                            if (parameter.incremental) {
                                generator.dispose()
                                return null
                            } else {
                                return generator
                            }
                        }
                    })

                return json
            }
        }

    }

    private fun checkSkin() {
        // Check all expected styles are all present - fail fast
        for (displayDesignation in DisplayDesignation.entries) {
            for (styleName in StyleName.entries) {
                require(
                    skin.has(
                        displayDesignation.styleName(styleName.toString()),
                        styleName.clazz.java
                    )
                )
                { "Skin is missing ${displayDesignation.styleName(styleName.toString())} of type ${styleName.clazz}" }
            }
        }
        for (drawableName in DrawableName.entries) {
            require(skin.has(drawableName.toString(), Drawable::class.java))
            { "Skin is missing $drawableName of type drawable" }
        }
    }

    private fun chooseDisplayDesignation(): DisplayDesignation {
        for (dd in DisplayDesignation.entries.sortedByDescending { it.factor() }) {
            if (dd.fits(Gdx.graphics.width, Gdx.graphics.height)) return dd
        }
        return DisplayDesignation.ZERO_23M9
    }

    private fun createBuildings() {
        val map = TmxMapLoader().load("Buildings.tmx")
        val renderer = OrthogonalTiledMapRenderer(map, 1 / 16f)
        val camera = OrthographicCamera()
        val batch = SpriteBatch()
        camera.setToOrtho(false)
        camera.zoom = displayDesignation.zoom(0.0625f)

        // Find a building
        val objectLayer = map.layers["Zones"]
        for (obj in objectLayer.objects.filter { it.name?.startsWith("House") != null }) {
            val house: RectangleMapObject = obj as RectangleMapObject
            val fbo =
                FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.width, Gdx.graphics.height, false)
            camera.position.set(house.rectangle.getCenter(Vector2()).scl(1 / 16f), 0f)
            camera.update()
            println("Camera set to ${camera.position} for ${house.name}")

            fbo.begin()
            Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
            renderer.setView(camera)
            renderer.render()
            batch.projectionMatrix = camera.combined
            batch.begin()
            batch.end()

            fbo.end()
            val texture = fbo.colorBufferTexture

            val bot = house.rectangle.getPosition(Vector2())
            val botProj = camera.project(Vector3(bot.scl(1 / 16f), 0f))
            val top =
                house.rectangle.getPosition(Vector2()).add(house.rectangle.getSize(Vector2()))
            val topProj = camera.project(Vector3(top.scl(1 / 16f), 0f))

            screenGrab = TextureRegion(
                texture,
                botProj.x.toInt(),
                botProj.y.toInt(),
                topProj.x.toInt() - botProj.x.toInt(),
                topProj.y.toInt() - botProj.y.toInt()
            )
            screenGrab.flip(false, true)

            val building = Building(obj.properties.get("Name", String::class.java), house.rectangle.width.toInt()/16)
            buildings.add(building)
            buildingTextures[building] = screenGrab
            buildingTiles[building] = loadTiles(map, house)
        }
    }

    private fun loadTiles(
        map: TiledMap,
        house: RectangleMapObject
    ): Pair<Array<Array<out Int?>>, Array<Array<out Int?>>> {
        val lowerLayer = map.layers["BuildingsLower"] as TiledMapTileLayer
        val higherLayer = map.layers["BuildingsHigher"] as TiledMapTileLayer
        val higherTiles: MutableList<Array<Int?>> = mutableListOf()
        val lowerTiles: MutableList<Array<Int?>> = mutableListOf()

        for (x in house.rectangle.x.toInt() ..< (house.rectangle.x + house.rectangle.width).toInt() step 16) {
            val lowerRow: MutableList<Int?> = mutableListOf()
            val higherRow: MutableList<Int?> = mutableListOf()

            for (y in house.rectangle.y.toInt() ..< (house.rectangle.y + house.rectangle.height).toInt() step 16) {
                val scaleX = x / 16
                val scaleY = y / 16
                lowerRow.add(lowerLayer.getCell(scaleX, scaleY)?.tile?.id)
                higherRow.add(higherLayer.getCell(scaleX, scaleY)?.tile?.id)
            }

            lowerTiles.add(lowerRow.toTypedArray())
            higherTiles.add(higherRow.toTypedArray())
        }
        return Pair(lowerTiles.toTypedArray(), higherTiles.toTypedArray())
    }

    fun getTexture(building: Building): TextureRegion {
        return buildingTextures[building]!!
    }

    fun getTiles(building: Building) : Pair<Array<Array<out Int?>>, Array<Array<out Int?>>> {
        return buildingTiles[building]!!
    }
}