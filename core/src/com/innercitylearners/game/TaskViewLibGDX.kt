// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

class TaskViewLibGDX(private val viewConfigLibGDX: ViewConfigLibGDX,
                     private val skin: Skin,
                     private val stageProvider: StageProvider) : TaskView {
    private var submitButton = TextButton("Submit", skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonDefault))
    private var resultLabel = Label("", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupH1))
    private var inputBoxes: List<TextField> = listOf()
    private val table = Table()
    private var questiontable = Table()
    private val backButton = TextButton("<", skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonLearningGroup))

    init {
        table.setFillParent(true)
        table.background = skin.getDrawable("color-black-80")
        backButton.pad(viewConfigLibGDX.styleRem(0.2f), viewConfigLibGDX.styleRem(1.25f), viewConfigLibGDX.styleRem(0.5f), viewConfigLibGDX.styleRem(1.25f))
        submitButton.pad(viewConfigLibGDX.styleRem(1.3f), viewConfigLibGDX.styleRem(2.5f), viewConfigLibGDX.styleRem(1f), viewConfigLibGDX.styleRem(2.5f))
    }
    override fun show() {
        stageProvider.addTable(table)
    }

    override fun addQuestion(first: Int, second: Int, answer: Int) {
        val firstQTable = Table()
        val firstQuestionLabel = Label("$first x $second", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupH1))
        val answerTextInput = TextField("", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.TextFieldTaskBody))
        firstQTable.add(firstQuestionLabel).pad(viewConfigLibGDX.styleRem(0.7f))
        firstQTable.add(answerTextInput).pad(viewConfigLibGDX.styleRem(0.7f))
        questiontable.add(firstQTable)
        questiontable.row()

        inputBoxes = inputBoxes + answerTextInput
    }

    override fun addSubmit(callBack: (answers: List<Int>) -> Unit) {
        resultLabel.color = skin.getColor("black")
        table.add(resultLabel)
        table.row()

        submitButton.addListener( object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                submitButton.isVisible = false
                // TODO: Validation
                callBack(inputBoxes.map { try {it.text.toInt()} catch (e: NumberFormatException) { -1 } })
            }
        } )
        table.add(submitButton).pad(10f)
    }

    override fun markCorrect(index: Int, answerCorrect: Boolean) {
        val delay = 1f
        val qColor = if (answerCorrect) Color.GREEN else Color.RED
        inputBoxes[index].addAction(Actions.color(qColor, delay))
    }

    override fun success() {
        resultLabel.setText("Well done!")
        resultLabel.addAction(Actions.color(Color.GREEN, 1f))
    }

    override fun fail() {
        resultLabel.setText("Not quite!")
        resultLabel.addAction(Actions.color(Color.RED, 1f))
    }

    override fun reset() {
        resultLabel.setText("")
        resultLabel.color = skin.getColor("black")
        inputBoxes.forEach { it.text = "" ; it.color = Color.GRAY }
        submitButton.isVisible = true
    }

    override fun addBack(back: () -> Unit) {
        backButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                table.remove()
                back()
            }
        })
        table.add(backButton).left().pad(viewConfigLibGDX.styleRem(0.7f))
        table.row()
        table.add(questiontable).expand()
        table.row()
    }
}
