// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class LearningGroupPresenter(viewConfig: ViewConfig) {

    class LearningGroupBackState(private val learningGroupPresenter: LearningGroupPresenter) {
        // Back method, category, text
        private val backState = ArrayDeque<Triple<(LearningGroupPresenter) -> Unit, String?, String>>()
        private var finish: (() -> Unit)? = null

        fun goBack() {
            if(backState.size > 1) {
                backState.removeLast() // we always carry one more than we need
                val state = backState.removeLast()
                if (state.second != null) {
                    learningGroupPresenter.selectedCategory = state.second as String
                    learningGroupPresenter.learningPlanView.selectInitialCategory(state.second as String)
                } else {
                    learningGroupPresenter.selectedCategory = String()
                }
                state.first(learningGroupPresenter)
            } else {
                finish?.invoke()
            }
        }

        fun setExit(finish: () -> Unit) {
            this.finish = finish
        }

        fun addBack(back: (LearningGroupPresenter) -> Unit) {
            backState.addLast(Triple(back,null,""))
        }

        fun updateBack(text: String) {
            val s = backState.removeLast()
            backState.addLast(Triple(s.first, null, text))
        }

        fun updateBack(text: String, category: String) {
            val s = backState.removeLast()
            backState.addLast(Triple(s.first, category, text))
        }

        fun getCrumbs(): MutableList<String> {
            val crumbs = mutableListOf<String>()
            if(backState.size > 1) {
                for(state in backState.dropLast(1)) {
                    if (state.second != null) crumbs.add(state.second as String)
                    crumbs.add(state.third)
                }
            }
            // Show category if there is one as final crumb
            if(backState.size > 0 && backState.last().second != null) {
                crumbs.add(backState.last().second as String)
            }
            return crumbs
        }
    }

    private var selectedCategory = String()
    private val learningGroupBackState = LearningGroupBackState(this)
    private val learningPlanView = viewConfig.createLearningPlanView()
    // Category, Description, Callback
    private val categoryGroups : MutableMap<String,MutableList<Triple<String,String,(LearningGroupPresenter) -> Unit>>> = mutableMapOf()

    fun exitAction(finished: () -> Unit) {
        learningPlanView.addExit {
            learningPlanView.hide()
            finished()
        }
        learningGroupBackState.setExit {
            learningPlanView.hide()
            finished()
        }
        learningPlanView.addBack {
            learningGroupBackState.goBack()
        }

    }
    fun backAction(back: (LearningGroupPresenter) -> Unit) {
        learningGroupBackState.addBack(back)
    }

    fun show() {
        learningPlanView.show()
    }

    fun addTask(text: String, description: String = String(), callBack: () -> Unit) {
        learningPlanView.addTask(text, description) {
            learningGroupBackState.updateBack(text)
            callBack()
        }
    }


    fun addGroup(text: String, description: String = String(), callBack: (LearningGroupPresenter) -> Unit) {
        // Assumption here that nothing else in the group has category either
        learningPlanView.addGroup(text, description) {
            learningGroupBackState.updateBack(text)
            callBack(this)
        }
    }

    fun addGroup(text: String, description: String = String(), category: String, callBack: (LearningGroupPresenter) -> Unit) {
        processCategoryGroup(text, description, category) {
            learningGroupBackState.updateBack(text, category)
            callBack(this) }
    }

    fun reset() {
        categoryGroups.clear()
        learningPlanView.reset()

        val crumbs = learningGroupBackState.getCrumbs()
        if(crumbs.size > 0) {
            learningPlanView.setBreadCrumb(crumbs.joinToString(" > "))
        }
    }

    fun addTitle(title: String) {
        learningPlanView.addTitle(title)
    }

    fun addDescription(description: String) {
        learningPlanView.addDescription(description)
    }

    private fun processCategoryGroup(text: String, description: String, category: String, callBack: (LearningGroupPresenter) -> Unit) {
        if(category !in categoryGroups) {
            if (categoryGroups.isEmpty() and selectedCategory.isBlank()) {
                learningPlanView.selectInitialCategory(category)
                selectedCategory = category
            }
            learningPlanView.addCategory(category) { displayCategory(category) }
            categoryGroups[category] = mutableListOf(Triple(text, description, callBack))
        } else {
            categoryGroups[category]!!.plus(Pair(text, callBack))
        }

        if (category == selectedCategory) {
            addGroupToView(text, description, callBack)
        }
    }

    private fun displayCategory(category: String) {
        selectedCategory = category
        for(group in categoryGroups[category]!!) {
            addGroupToView(group.first, group.second, group.third)
        }
    }

    private fun addGroupToView(title: String, description: String, callback: (LearningGroupPresenter) -> Unit) {
        learningPlanView.addGroup(title, description) {
            callback(this)
        }
    }
}