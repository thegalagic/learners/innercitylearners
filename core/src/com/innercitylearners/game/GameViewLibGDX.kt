// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.InputMultiplexer
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align

class GameViewLibGDX(private val viewConfigLibGDX: ViewConfigLibGDX,
                     private val skin: Skin,
                     private val mapRender: () -> Unit,
                     exit: () -> Unit) : Screen, GameView, StageProvider,
    GameInputMultiplexerLibGDX {


    private val gameActions: MutableList<Table> = mutableListOf()
    private var gameActionsHidden: Boolean = false
    private val multi: InputMultiplexer = InputMultiplexer()
    private val settingsButton = ImageButton(skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.ImageButtonIconSettings))
    private val memoryBankButton = ImageButton(skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.ImageButtonIconMemoryBank))
    private val mindButton = ImageButton(skin)
    private val buildButton = ImageButton(skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.ImageButtonIconBuild))
    private val heartsLabel: Label = Label("0", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelGameH1))
    private val starsLabel: Label = Label("0", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelGameH1))
    private val stage: Stage = Stage()
    private val table = Table(skin)

    init {
        table.padTop(viewConfigLibGDX.styleRem(.9f)).padBottom(viewConfigLibGDX.styleRem(.9f))
        table.setFillParent(true)
        stage.addActor(table)

        starsLabel.setAlignment(Align.center)

        table.add().colspan(2)
        createStatsLabel(table, heartsLabel, "Hearts", viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.TextureIconHeart))
        createStatsLabel(table, starsLabel, "Stars", viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.TextureIconStar))
        table.add().colspan(2)

        table.row().expand().colspan(6)
        table.add()
        table.row()

        table.add(Image()).expandX()
        settingsButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                exit.invoke()
            }
        })

        addControlButton(settingsButton, "Exit", table)
        addControlButton(memoryBankButton, "Memory Bank", table)
        addControlButton(mindButton, "Mind", table, false)
        addControlButton(buildButton, "Build", table)

        table.add(Image()).expandX()
    }

    override fun setScreen() {
        viewConfigLibGDX.screen = this
        multi.addProcessor(stage)
        widgetsVisible(true)
        Gdx.input.inputProcessor = multi
    }

    override fun setStars(total: Int) {
        starsLabel.setText("$total")
    }

    override fun setMemoryBank(function: () -> Unit) {
        memoryBankButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                function()
            }
        })
    }

    override fun setBuildToggle(toggle: () -> Unit) {
        buildButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                toggle()
            }
        })
    }


    override fun widgetsVisible(enabled: Boolean) {
        for(widget in gameActions) {
            widget.isVisible = enabled
        }
    }

    override fun hideGameActions() {
        if (!gameActionsHidden) {
            for(widget in gameActions) {
                widget.touchable = Touchable.disabled
                widget.addAction(Actions.fadeOut(.2f))
            }
        }
        gameActionsHidden = true
    }

    override fun showGameActions() {
        if (gameActionsHidden) {
            for(widget in gameActions) {
                widget.touchable = Touchable.enabled
                widget.addAction(Actions.fadeIn(.2f))
            }
        }
        gameActionsHidden = false
    }

    override fun show() {
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        mapRender()

        stage.act()
        stage.draw()
    }

    override fun resize(width: Int, height: Int) {
    }

    override fun pause() {
    }

    override fun resume() {
    }

    override fun hide() {
    }

    override fun dispose() {
        stage.dispose()
    }

    override fun addTable(table: Table) {
        stage.addActor(table)
    }

    private fun addControlButton(
        button: ImageButton,
        text: CharSequence,
        table: Table,
        visible: Boolean = true
    ) {
        val innerTable = Table()
        innerTable.isVisible = visible
        if (visible) gameActions.add(innerTable)
        innerTable.add(button).minWidth(viewConfigLibGDX.styleRem( 3.5f)).minHeight(viewConfigLibGDX.styleRem( 3.2f)).row()
        val controlLabel = Label(text, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelGameBody ))
        controlLabel.wrap = true
        controlLabel.setAlignment(Align.center)
        innerTable.add(controlLabel)
            .fill()
            .padTop(viewConfigLibGDX.styleRem( 0.35f))
            .minWidth(viewConfigLibGDX.styleRem( 4.5f))
        table.add(innerTable)
            .space(viewConfigLibGDX.styleRem( 0.35f),
                 viewConfigLibGDX.styleRem( 0.35f),
                 0f,
                 viewConfigLibGDX.styleRem( 0.35f))
            .top()
    }

    private fun createStatsLabel(table: Table, statsLabel: Label, text: String, drawableName: String) {
        val statsTable = Table()
        val label = Label(text, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelGameBody ))
        statsTable.add(label).colspan(2)
        statsTable.row()
        val image = Image(skin, drawableName)
        statsTable.add(image).padTop(viewConfigLibGDX.styleRem( -0.5f)).expand()
        statsTable.add(statsLabel)
            .padTop(viewConfigLibGDX.styleRem( -0.5f))
            .padRight(viewConfigLibGDX.styleRem( 0.5f))
            .padLeft(viewConfigLibGDX.styleRem( 0.5f))
        table.add(statsTable)
    }

    override fun addProcessor(inputAdapter: InputAdapter) {
        multi.addProcessor(inputAdapter)
    }

    override fun removeProcessor(inputAdapter: InputAdapter) {
        multi.removeProcessor(inputAdapter)
    }
}