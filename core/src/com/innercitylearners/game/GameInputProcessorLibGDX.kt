// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.math.Vector3

class GameInputProcessorLibGDX(private val cityView: CityView) : InputAdapter() {

    private lateinit var lastTouch: Vector3

    override fun scrolled(amountX: Float, amountY: Float): Boolean {
        cityView.zoom(.005f * amountY)
        return true
    }
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        lastTouch = Vector3(screenX.toFloat(), screenY.toFloat(),0f)
        return true
    }
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        cityView.touch(screenX, screenY)
        return true
    }
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        val newTouch = Vector3(screenX.toFloat(), screenY.toFloat(),0f)
        val delta = newTouch.cpy().sub(lastTouch).scl(-1f, 1f, 0f)
        cityView.translate(delta.x, delta.y)
        lastTouch = newTouch
        return true
    }

}
