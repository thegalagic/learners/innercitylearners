// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.EventListener
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align


class LearningGroupViewLibGDX(private val viewConfigLibGDX: ViewConfigLibGDX,
                              private val skin: Skin) : LearningGroupView {
    private val borderPadding = viewConfigLibGDX.styleRem(0.7f)
    private val breadCrumbLabel: Label = Label("", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupBody ))
    private val descriptionLabel: Label = Label("", skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupBody ))
    private var taskHorizontalGroup: HorizontalGroup
    private var selectedCategory: String = String()
    private var titleLabel: Label
    private var titleButton = TextButton("*", skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonLearningGroup))
    private lateinit var stageProvider: StageProvider
    private var table = Table()
    private var categorytable = Table()
    private val backButton = TextButton("<", skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonLearningGroup))
    private val exitButton = TextButton("X", skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonLearningGroup))

    init {
        table.setFillParent(true)
        table.pad(borderPadding)
        table.background = skin.getDrawable("color-black-80")

        backButton.pad(viewConfigLibGDX.styleRem(0.2f), viewConfigLibGDX.styleRem(1.25f), viewConfigLibGDX.styleRem(0.5f), viewConfigLibGDX.styleRem(1.25f))
        exitButton.pad(viewConfigLibGDX.styleRem(0.2f), viewConfigLibGDX.styleRem(1.25f), viewConfigLibGDX.styleRem(0.5f), viewConfigLibGDX.styleRem(1.25f))

        table.add(backButton).left().padBottom(viewConfigLibGDX.styleRem(0.7f))
        table.add(exitButton).right().padBottom(viewConfigLibGDX.styleRem(0.7f))
        table.row()

        // Title button, breadcrumb, title and description
        titleButton.touchable = Touchable.disabled
        table.add(titleButton).minHeight(viewConfigLibGDX.styleRem(12f)).growX().colspan(2)
        table.row()
        table.add(breadCrumbLabel).colspan(2).left().padTop(viewConfigLibGDX.styleRem(0.7f))
        table.row()
        titleLabel = Label(String(), skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.LabelLearningGroupH1))
        titleLabel.setWrap(true)
        table.add(titleLabel).growX().align(Align.left).colspan(2)
        table.row()
        descriptionLabel.wrap = true
        table.add(descriptionLabel).colspan(2).left().growX().padTop(viewConfigLibGDX.styleRem(0.35f))
        table.row()

        // Categories

        // ScrollPanes don't do alignment, so we have to wrap their content in another table
        // and specify growing cells in that to put our real content where we want it.
        val wrapCategoryTable = Table()
        wrapCategoryTable.add(categorytable)
        wrapCategoryTable.add().growX()

        val categoryScrollPane = ScrollPane(wrapCategoryTable, skin)
        categoryScrollPane.setClamp(true)
        categoryScrollPane.setOverscroll(false, true)
        categoryScrollPane.setupOverscroll(
            viewConfigLibGDX.styleRem(1f),
            viewConfigLibGDX.styleRem(1f),
            viewConfigLibGDX.styleRem(7f))
        categoryScrollPane.setScrollingDisabled(true, false)

        table.add(categoryScrollPane).growX().colspan(2).pad(viewConfigLibGDX.styleRem(0.6f),0f,viewConfigLibGDX.styleRem(0.6f),0f)
        table.row()

        // Groups and Tasks

        taskHorizontalGroup = HorizontalGroup()
        taskHorizontalGroup.align(Align.left)
        taskHorizontalGroup.expand()
        taskHorizontalGroup.fill()
        taskHorizontalGroup.pad(viewConfigLibGDX.styleRem(0.7f), 0f, 0f, 0f)
        taskHorizontalGroup.rowAlign(Align.left)
        taskHorizontalGroup.wrap()

        // ScrollPanes don't do alignment, so we have to wrap their content in another table
        // and specify growing cells in that to put our real content where we want it.
        val wrapTaskTable = Table()
        wrapTaskTable.add(taskHorizontalGroup).growX()
        wrapTaskTable.row()
        wrapTaskTable.add().growY()

        val taskScrollPane = ScrollPane(wrapTaskTable, skin)
        taskScrollPane.setClamp(true)
        taskScrollPane.setOverscroll(true, false)
        taskScrollPane.setupOverscroll(
            viewConfigLibGDX.styleRem(1f),
            viewConfigLibGDX.styleRem(1f),
            viewConfigLibGDX.styleRem(7f))

        table.add(taskScrollPane).grow().colspan(2)
    }

    override fun addTitle(title: String) {
        titleLabel.setText(title)
    }

    override fun addDescription(description: String) {
        descriptionLabel.setText(description)
    }

    override fun addExit(finished: () -> Unit) {
        exitButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                finished()
            }
        })
    }

    override fun addBack(back: () -> Unit) {
        backButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                back()
            }
        })
    }

    override fun addGroup(text: String,
                          description: String,
                          callBack: () -> Unit) {
        addTask(text, description, object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                callBack()
            }
        })
    }

    override fun addTask(text: String,
                         description: String,
                         callBack: () -> Unit) {
        addTask(text, description, object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                hide()
                callBack()
            }
        })
    }

    override fun setBreadCrumb(breadCrumb: String) {
        breadCrumbLabel.setText(breadCrumb)
        breadCrumbLabel.isVisible = true
    }

    override fun addMyViewParent(stageProvider: StageProvider) {
        this.stageProvider = stageProvider
    }

    override fun reset() {
        categorytable.clear()
        categorytable.isVisible = false
        taskHorizontalGroup.clear()
        breadCrumbLabel.isVisible = false
    }

    override fun hide() {
        table.remove()
    }

    override fun show() {
        stageProvider.addTable(table)
    }

    override fun addCategory(category: String, callback: () -> Unit) {
        categorytable.isVisible = true
        val label = Label(category, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupBody))
        label.color = when (category) {
            selectedCategory -> skin.getColor("contessa")
            else -> skin.getColor("halfspanishwhite")
        }

        label.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                for(cell in categorytable.cells) {
                    if(cell.actor != label) {
                        cell.actor.addAction(Actions.color(skin.getColor("halfspanishwhite"), .2f))
                    }
                }
                label.addAction(Actions.color(skin.getColor("contessa"), .2f))
                taskHorizontalGroup.clear()
                callback()
            }
        })
        categorytable.add(label).pad(viewConfigLibGDX.styleRem(0.1f), 0f, viewConfigLibGDX.styleRem(0.1f), viewConfigLibGDX.styleRem(1.7f))
    }

    override fun selectInitialCategory(category: String) {
        selectedCategory = category
    }

    private fun addTask(title: String, description: String, listener: EventListener) {
        val taskRightPadding = viewConfigLibGDX.styleRem(0.7f)
        val taskTable = Table()
        taskTable.padRight(taskRightPadding)
        taskTable.padBottom(viewConfigLibGDX.styleRem(0.7f))
        taskTable.align(Align.left)

        val textButton = TextButton("*", skin, viewConfigLibGDX.styleName(ViewConfigLibGDX.StyleName.TextButtonLearningGroup))
        textButton.addListener(listener)
        // taskWidth = width of screen - padding between two tasks - borders - little wiggle room
        var taskWidth = (Gdx.graphics.width - (2*taskRightPadding) - (2*borderPadding) - viewConfigLibGDX.styleRem(0.5f)) / 2
        taskWidth = taskWidth.coerceAtMost(viewConfigLibGDX.styleRem(11.5f))
        val taskHeight = taskWidth/2
        taskTable.add(textButton).minHeight(taskHeight).minWidth(taskWidth)

        taskTable.row()
        val titleLabel = Label(title, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupH2))
        titleLabel.wrap = true
        taskTable.add(titleLabel).align(Align.topLeft).growX().padTop(viewConfigLibGDX.styleRem(0.3f))

        if(description.isNotBlank()) {
            taskTable.row()
            val descLabel = Label(description, skin, viewConfigLibGDX.styleName( ViewConfigLibGDX.StyleName.LabelLearningGroupBody))
            descLabel.setWrap(true)
            taskTable.add(descLabel).growX().padTop(viewConfigLibGDX.styleRem(0.3f))
        }

        taskHorizontalGroup.addActor(taskTable)
    }
}