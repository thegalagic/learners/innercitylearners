// SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// An educational city building game for children in primary education.
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

package com.innercitylearners.game

class BuildingPresenter(viewConfig: ViewConfig) {
    private lateinit var finishAction: () -> Unit
    private lateinit var callBack: (Building) -> Unit
    private val buildingView = viewConfig.createBuildingView(this::chosen)

    fun show(callBack: (Building) -> Unit) {
        buildingView.show()
        this.callBack = callBack
    }

    fun addBuilding(building: Building, name: String) {
        buildingView.addBuilding(building, name)
    }

    fun exitAction(finished: () -> Unit) {
        this.finishAction = finished
        buildingView.addExit { allDone() }
    }

    private fun chosen(building: Building) {
        callBack(building)
        allDone()
    }

    private fun allDone() {
        buildingView.hide()
        finishAction()
    }
}