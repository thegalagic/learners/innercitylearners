<!--
SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

An educational city building game for children in primary education.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Contributor Setup

## Initial Setup

Please install [Android Studio Koala](https://developer.android.com/studio/)
and the Android SDK.

We are using [devenv.sh](https://devenv.sh) to manage the dev environment. Follow
their instructions to get setup first. I suggest using the `direnv` integration
for the smoothest experience.

Create an optional `devenv.local.nix` if you want to customise build output by
changing the env variable `BUILDDIR` (build artifacts, logs etc). For example:

```nix
{
  # Use expanded paths here as nix, ninja and BASH cannot (simply)
  env.BUILDDIR = "/home/me/builds/innercitylearners";
}
```

Create an `other.properties` file to sign release builds with the following
content:

```gradle
secure_files.dir=/dir/of/release-keystore.jks and release-keystore.properties
```

The `secure_files.dir` setting should point to a dir with keystore and properties
files called `release-keystore.jks` and `release-keystore.properties` for signing.
The properties file should contain:

```properties
keyAlias=release
keyPassword=password
storePassword=password
```

Run `ninja` to do a build. Run `pre-commit run -a` to run the pre-commit hooks
manually.

## Testing

Running the default build will execute everything we care about:

```bash
ninja
```

To test the CI build (which will catch any environment issues) you can use:

```bash
# ...with podman
gitlab-ci-local \
  --container-executable podman \
  --volume "$(grep -oP "(?<==).*" other.properties)/:/gcl-builds/.secure_files/:z"
# with docker
gitlab-ci-local \
  --volume "$(grep -oP "(?<==).*" other.properties)/:/gcl-builds/.secure_files/:ro"
```

This mounts your keystore (specified in `other.properties`) where the CI build
expects to find it.

## Updating Dependencies

First the simple stuff:

- You can run `ninja update` to fetch the latest version of our copier template
  for [galagos](https://gitlab.com/thegalagic/galagos).
- To update devenv use `devenv update`. If the version of android NDK gets
  updated `ls $ANDROID_NDK_ROOT` you'll need to update it in `android/build.gradle`

Next it's best to update Android Studio first then use its code
analysis recommendations to update other components such as the Gradle version
and the Android Gradle Plugin (AGP) automatically. If you do update Android Studio
also update these instructions to make it clear which version developers need.

Then the following versions are controlled through the gradle build files:

- Kotlin version
- LibGDX version (and extensions)
- Gradle toolchain(s)

There is a relationship between toolchain version (i.e. JVM) and the Kotlin language
version [Which versions of Kotlin are compatible with which versions of Java?](https://stackoverflow.com/questions/63989767/which-versions-of-kotlin-are-compatible-with-which-versions-of-java)
Until Kotlin 2.0.0 we cannot go beyond toolchain 21.

Attention must also be paid to [libGDX and the versions](https://libgdx.com/dev/versions/)
it supports.

We can also update the minimum, compile and target android SDK versions which is
controlled by `build.gradle` in the android project. Changes here need to also
be reflected in the packages in `devenv.nix` that install the android SDK.

The version of [gradle plugin org.gradle.toolchains.foojay-resolver](https://plugins.gradle.org/plugin/org.gradle.toolchains.foojay-resolver)
(required for using toolchains) is controlled in `settings.gradle`.

If you update the toolchain you'll also need to:

- Update the run/debug configurations in Android Studio to use the same JDK.
- Update the jdk package in `devenv.nix` to be the same. In theory gradle should
  download what it needs but it seems to fail in the devenv shell to unpack
  them. "Unable to download toolchain matching the requirements...a problem
  occurred starting process..." it leaves unpacked jdks in `~/.gradle/jdks`
  perhaps some other tool is missing like `unzip`. Luckily it detects the local
  nix-installed jdk fine and will use that. I think this is preferable overall.
- Check the README and any other installation instructions if we need to update
  the version of Java users need to have to run the game.

We are not specifying a vendor of the toolchain at the moment. Under nix's JDK
the package vendor is `N/A` (`java -XshowSettings:properties -version`). We
definitely have an inconsistency between exact toolchain used by Android Studio
and that by the build and CI. No easy way to get Android Studio to use JDK that
nix has installed though.

We also have a dependency in `.gitlab-ci.yml` of which NixOS image tag to use.
