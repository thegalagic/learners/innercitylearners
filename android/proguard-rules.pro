# SPDX-FileCopyrightText: 2024 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: CC-BY-SA-4.0
#
# An educational city building game for children in primary education.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/learners/innercitylearners/AUTHORS.md)
#
# This work is licensed under the Creative Commons Attribution 4.0 International
# License. You should have received a copy of the license along with this work.
# If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
# Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-verbose

-dontwarn com.badlogic.gdx.backends.android.AndroidFragmentApplication

# Don't warn about below classes we might use in future
#noinspection ShrinkerUnresolvedReference

# Required if using Gdx-Controllers extension
-keep class com.badlogic.gdx.controllers.android.AndroidControllers

# Required if using Box2D extension
-keepclassmembers class com.badlogic.gdx.physics.box2d.World {
   boolean contactFilter(long, long);
   void    beginContact(long);
   void    endContact(long);
   void    preSolve(long, long);
   void    postSolve(long, long);
   boolean reportFixture(long);
   float   reportRayFixture(long, float, float, float, float, float);
}

# Required if using libGDX Scene2d Skins (JSON Skin descriptors)
# [Added Proguard/R8 rules for Scene2d Skins by obigu · Pull Request #7375 · libgdx/libgdx](https://github.com/libgdx/libgdx/pull/7375)
-keep public class com.badlogic.gdx.scenes.scene2d.** { *; }
-keep public class com.badlogic.gdx.graphics.g2d.BitmapFont { *; }
-keep public class com.badlogic.gdx.graphics.Color { *; }

# As we are using a FreeType font (Press Start 2P)
-keep public class com.badlogic.gdx.graphics.g2d.freetype.** { *; }